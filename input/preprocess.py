# Process individual instruments datasets and merge them

import numpy as np
import pandas as pd
import librosa
from os import listdir

instruments_encoding = {"bass": 0, 
						"brass": 1, 
						"flute": 2, 
						"guitar": 3, 
						"keyboard": 4, 
						"mallet": 5, 
						"organ": 6, 
						"reed": 7, 
						"string": 8, 
						"synth_lead": 9,
						"vocal": 10
						}

# Swich between processing and merging datasets
merge = False
# Switch between possible options [train, test, arrays]
testing = False
# Switch between processing for VAE and direct classification
for_vae = True
vae_raw = True

specific_sr = True

raw_limit = 1000000

data_array_full = []

data_key_full = []

sr_global = None

if specific_sr:
	sr_global = 1000



if testing:
	source_path = './testing/'
	datasets_path = './test_datasets/'
	result_name = 'full_test_dataset.csv'
	vae_result_name = 'music_array_test.npy'
	vae_raw_name = 'music_raw_test1000.npy'
	vae_key_name = 'key_array_test.npy'
	vae_raw_key_name = 'key_raw_test1000.npy'
	error_log = 'error_log_testing.txt'
else:
	source_path = './reduced/'
	datasets_path = './datasets/'
	result_name = 'full_dataset.csv'
	vae_result_name = 'music_array.npy'
	vae_raw_name = 'music_raw1000.npy'
	vae_key_name = 'key_array.npy'
	vae_raw_key_name = 'key_raw1000.npy'
	error_log = 'error_log.txt'

error_files = []

def single_feature_extract(file_name):
    """
    Define function that takes in a file an returns features in an array

    Inspired by https://medium.com/@nadimkawwa/can-we-guess-musical-instruments-with-machine-learning-afc8790590b8
    """

    y, sr = librosa.load(file_name,sr=sr_global)

    y_harmonic, y_percussive = librosa.effects.hpss(y)
    if np.mean(y_harmonic)>np.mean(y_percussive):
        harmonic=1
    else:
        harmonic=0

    features = [harmonic]
        
    mfcc = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=13)
    mfcc=np.mean(mfcc, axis=1)

    features.extend(mfcc)
    
    
    spectrogram = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=128,fmax=8000)  
    spectrogram = np.mean(spectrogram, axis=1)

    features.extend(spectrogram)

    chroma = librosa.feature.chroma_cens(y=y, sr=sr)
    chroma = np.mean(chroma, axis=1)

    features.extend(chroma)
    
    contrast = librosa.feature.spectral_contrast(y=y, sr=sr)
    contrast = np.mean(contrast, axis=1)

    features.extend(contrast)
    
    return features



def create_basic_dict() -> dict:
	expeted_features = {'harmonic': 1,
	 					'mfcc' : 13, 
	 					'spectrogram': 128,
	 					'chroma': 12,
	 					'contrast': 7, 
	 					'instrument': 1}

	data_dict = dict()

	for key, value in expeted_features.items():
		if value != 1:
			for index in range(value):
				data_dict[str(key) + "_" + str(index)] = []
		else:
			data_dict[key] = []

	return data_dict


def create_single_dataset(instrument_type: str):
	"""
	Creates dataset from single instrument type
	"""

	
	data_dict = create_basic_dict()

	instrument_files = listdir(source_path + instrument_type)

	file_count = len(instrument_files)

	for ind, ins_file in enumerate(instrument_files):
		full_path = source_path + instrument_type + "/" + ins_file
		# print("Processing file", full_path, "which is", ind + 1, "from", file_count)
		try:
			features = single_feature_extract(full_path)
		except:
			print("File skipped due to file encoding error | logged")
			error_files.append(full_path)
			continue

		features.append(instruments_encoding.get(instrument_type, -1))

		for index, key in enumerate(data_dict.keys()):
			data_dict[key].append(features[index])


	dataset = pd.DataFrame(data_dict)

	dataset.to_csv(datasets_path + instrument_type + "_processed.csv", index=False)


def process_data_into_array(instrument_type: str):
	"""
	Process data into numpy array
	"""

	instrument_files = listdir(source_path + instrument_type)

	file_count = len(instrument_files)

	for ind, ins_file in enumerate(instrument_files):
		if vae_raw and raw_limit <= ind:
			break

		full_path = source_path + instrument_type + "/" + ins_file
		# print("Processing file", full_path, "which is", ind + 1, "from", file_count)

		try:
			data, sr = librosa.load(full_path, sr=sr_global)

			if not vae_raw:
				spectrogram = librosa.feature.melspectrogram(y=data, sr=sr, n_mels=132) 
		except:
			print("Data failed to open:", full_path)
			continue

		if vae_raw:
			data_array_full.append(data)
		else:
			data_array_full.append(spectrogram)

		data_key_full.append(instruments_encoding.get(instrument_type, -1))



def process_all_instruments():
	"""
	Produces dataframes for all instrument types
	"""

	amount_of_instruments = len(instruments_encoding.keys())

	for ind, key in enumerate(instruments_encoding.keys()):
		print("Processing instument type:", key, "which is", ind + 1, "from", amount_of_instruments)
		if for_vae:
			process_data_into_array(key)
		else:
			create_single_dataset(key)


def merge_datasets(result_name: str):
	datasets = listdir(datasets_path)

	datasets_number = len(datasets)

	full_dataset = pd.DataFrame(create_basic_dict())

	for ind, dataset in enumerate(datasets):
			full_path = datasets_path + dataset
			print("Processing", dataset, "which is", ind + 1, "from", datasets_number)

			full_dataset = full_dataset.append(pd.read_csv(full_path))

	full_dataset.to_csv(datasets_path + result_name, index=False)



if __name__ == "__main__":
	if merge:
		merge_datasets(result_name)
	else:
		process_all_instruments()

	if for_vae:
		data_array_full = np.array(data_array_full)
		data_key_full = np.array(data_key_full)
		if vae_raw:
			np.save(datasets_path + vae_raw_name, data_array_full)
			np.save(datasets_path + vae_raw_key_name, data_key_full)
		else:
			np.save(datasets_path + vae_result_name, data_array_full)
			np.save(datasets_path + vae_key_name, data_key_full)


	with open(error_log, 'w') as f:
	    for item in error_files:
	        f.write("%s\n" % item)
