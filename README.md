## Repository with semestral work for subject NI-MVI

### Main topic
Test hypothesis whether an encoding of musical instrument sounds into a latent vectors using variational autoencoders can improve their classification in a meaningfull way

### Repository structure
* `articles` -> Some articles that I have downloaded, not neceaserally all that I have read
* `code`
	* `MusicAutoEncoders.ipynb` -> Main ipython notebook exported from Google Colab
	* `models` -> Saved weigths of the trained models
	* `deprecated` -> Code that I have tested for previous assignment
		* `vq_vae.ipynb` -> Test of VQ VAE
* `input` -> Folder containing small subsets of data in order to keep the repository size within reason
	* `preprocess.py` -> Code used for preprocessing of the raw data
	* `test_datasets` -> Folder containing processed test dataset for musical instruments clasification
* `output` -> Folder containing samples of output
	* `deep_AE_sample` -> Folder with samples using deep Auto Encoder
		* `input_sound_AE.wav` -> Sample of input sound into the deep AE on spectrogram data
		* `output_sound_AE.wav` -> Sample of output sound from the deep AE on spectrogram data
		* `input_sound_AE_raw.wav` -> Sample of input sound into the deep AE on raw data
		* `output_sound_AE_raw.wav` -> Sample of output sound from the deep AE on raw data
	* `VAE_sample` -> Folder with samples using Variational Auto Encoder
		* `input_sound_VAE.wav` -> Sample of input sound into the VAE on spectrogram data
		* `output_sound_VAE.wav` -> Sample of output sound from the VAE on spectrogram data
		* `input_sound_VAE_raw.wav` -> Sample of input sound into the VAE on raw data
		* `output_sound_VAE_raw.wav` -> Sample of output sound from the VAE on raw data
	* `model_results` -> Folder containing accuracy results of classification models
		* `mod_res.csv` -> CSV file containing accuracy of all classification models on various data
		* `generate_graph.ipynb` -> Short notebook for creating graph from `mod_res.csv` file
	* `latent_vectors` -> Folder containing latent vectors encoding of the data produced from deep autoencoders and variational autoencoders
		* `AE_latent.npy` -> Latent encoding using deep AE on spectrogram data
		* `raw_AE_latent.npy` -> Latent encoding using deep AE on raw data
		* `raw_latent_keys.npy` -> Raw data encoding keys (ground truth labels)
		* `raw_VAE_latent.npy` -> Latent encoding using VAE on raw data
		* `spec_latent_keys.npy` -> Spectrogram data encoding keys (ground truth labels)
		* `test_latent_vector_keys.npy` -> Test data latent encoding keys (ground truth labels)
		* `test_latent_vectors.npy` -> Test data latent encoding using deep AE
		* `VAE_latent.npy` -> Latent encoding using VAE on spectrogram data
* `text` -> Folder containing documentation and reports for this semestral work
	* `process_report.pdf` -> Preliminary progress report
	* `process_report.tex` -> LaTex source for preliminary progress report
	* `img` -> Folder containing images used in final report
	* `report.pdf` -> Final report
	* `report.tex` -> LaTex source for final report
* `README.md` -> Description of repository