%% 
%% Created in 2018 by Martin Slapak
%%
%% Based on file for NRP report LaTeX class by Vit Zyka (2008)
%%
%% Compilation:
%% >pdflatex report
%% >bibtex report
%% >pdflatex report
%% >pdflatex report

\documentclass{mvi-report}

\usepackage[utf8]{inputenc} 

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\title{Music instrument classification on latent vector representation}

\author{Tomáš Vlk}
\affiliation{ČVUT - FIT}
\email{vlktoma5@fit.cvut.cz}

\def\file#1{{\tt#1}}

\begin{document}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Working with music is currently a trendy topic in the field of machine learning, especially in field of music generation. This was motivated by a successes of GAN\footnote{Generative adversarial networks} and VAE\footnote{Variational auto encoder} in generating images. Music is much more high dimensional than even high resolution images, therefore it requires much more computational power in order to achieve respectable results. Currently trending article is \citep{dhariwal2020jukebox}\par

Due to this severe computational requirements, I have chosen much simpler topic of classifying musical instruments and test whether an encoding of their sounds into a latent vectors using a VAE will lead into a significant improvement or not.\par 

Their are many articles that aim to solve the task of encoding music into a latent vectors such as \citep{brunner2018midi} or \citep{roberts2018hierarchical}. Most of them however use music in MIDI\footnote{Musical Instrument Digital Interface} format which represents specific tones in form which is closer to classical musical notes than other music formats.\par

I have chosen to instead try to use a WAV\footnote{Wave form audio} format of audio recordings.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Input data}
I have used dataset NSynth from 

https://magenta.tensorflow.org/datasets/nsynth, that contains 289 thousand musical instruments recordings from 11 different instruments (it could be separated further but I didn't want to do that). There is an issue that the amount of individual instruments recordings is highly unbalanced, therefore I have extracted first 4500 recordings from each instrument class. For final testing of estimated "real-life" performance I have used complete test part of the NSynth dataset, as it is available at \url{https://magenta.tensorflow.org/datasets/nsynth}. This part of the dataset contains 4096 examples from various instrument classes. Only class that is missing in test dataset is "synth vocal".\par

Available instrument classes where,

\begin{itemize}
\item bass
\item brass
\item flute
\item guitar
\item keyboard
\item mallet
\item organ
\item reed
\item string
\item synth lead
\item vocal
\end{itemize}

I have done multiple variations of preprocessing the data for different usages. Those where,

\begin{itemize}
\item Feature extraction for classification
\item Spectrogram extraction for latent vector encoding
\item Converting into a raw numpy array
\end{itemize}

I will now provide description of all those methods.

\subsection{Feature extraction for classification}
\label{subsec:FeatureExt}
In order to get some baseline to compare classification of latent vector encodings, I need to train some models on non encoded data. After searching trough multiple sources how the classification is usually done, such as \citep{eronen2000musical} or more modern \citep{solanki2019music}, I have decided to extract features from the audio, create a one dimensional array from them and use them to train the models and find out their accuracy.\par

The extracted features where,

\begin{itemize}
\item whether the instrument was harmonic or not
\item mean of mel-frequency cepstrum
\item mean of spectrogram
\item mean of normalized chroma energy
\item mean of spectral contrast
\end{itemize}

where all means had multiple values as the signal developed through time. One audio sample therefore has shape of one dimensional vector with length of 161.

\subsection{Spectrogram extraction for latent vector encoding}
\label{subsec:SpecExt}
I have chosen spectrogram data, as a first variant of audio representation that could be used as input for VAE. Therefore I have extracted spectrogram data from each audio recording individually, with the recording native sampling rate, which is 16000. Single audio recording in form of two dimensional vector of lengths 132 and 126.\par 

My motivation in choosing the spectrogram representation as an input of VAE was mainly driven by the two dimensional vector representation of the data. Due to this fact I can use similar methods and techniques as in case of encoding visual data such as images.

\subsection{Converting into a raw numpy array}
\label{subsec:RawExt}
Last but not least, it would be interesting to see how the VAE will preform on raw music data. Therefore I have also converted the audio data into a numpy array. Due to reasons that will be explained latter in Section \ref{sec:methods}, I had to reduce the sampling rate severely from 16000 to 1000, that way the single audio files is represented with one dimensional vector of length 4000, instead of length 64000.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methods}
\label{sec:methods}
There are two main parts to this task for which I had two choose very different methods and models. Two two parts are the \emph{classification task} and the the \emph{encoding task}. Therefore I will describe each of those methods in their individual subsections.

\subsection{Classification methods}
In order to be able to compare performance of the classification models on both feature extracted data, as described in Section \ref{subsec:FeatureExt}, and also on encoded data, I had chosen to use the same models for both.\par

The models used where the following,

\begin{itemize}
\item Gaussian naive bayes
\item Random forest
\item AdaBoost
\item XGBoost
\item SVM
\item Fully connected neural network with batch normalization
\end{itemize}

\begin{figure}[hbt!]
 \centering
 \includegraphics[scale=0.5]{img/fcnn.png}
 \caption{Architecture of fully connected neural network with batch normalization}
 \label{fig:fcnn_arch}
\end{figure}

All models, except the neural net, used their implementation in python library sklearn, with their default hyper-parameters. The neural net was implemented using Keras and Tensorflow. The architecture of the fully connected neural net with batch normalization can be seen in Figure \ref{fig:fcnn_arch}.\par 

All data, that was used for training those models where normalized using sklearn min max scaler.

\subsection{Encoding methods}

I have used two main methods for encoding the data in latent vectors. First the already stated VAE since my aim was to test its performance. But I have also chosen a deep auto encoder as a baseline to see if the performance of VAE is better than that.\par

For each of these models, I had to do two separate variants, one for encoding of spectrogram data and other one for raw data, as described in Sections \ref{subsec:SpecExt} and \ref{subsec:RawExt} respectively.

\begin{figure}
 \centering
 \includegraphics[scale=0.4]{img/spec_deep_ae.png}
 \caption{Architecture of the deep auto encoder for spectrogram data}
 \label{fig:spec_deep_ae_arch}
\end{figure}

\begin{figure}
 \centering
 \includegraphics[scale=0.4]{img/encoder_raw_VAE.png}
 \caption{Architecture of the VAE encoder for raw data}
 \label{fig:raw_vae_encoder_arch}
\end{figure}

Architecture of the deep autoencoder used for spectrogram data can be seen in Figure \ref{fig:spec_deep_ae_arch}, and architecture of the encoder part of the VAE that was used for encoding raw data, can be seen \ref{fig:raw_vae_encoder_arch}. I have not included the other diagrams, since it would take up too much space.\par

Since the spectrogram data is two dimensional I have used two dimensional convolutional layers and two dimensional upsampling and max pooling layers. I have selected the amount of neurons in individual layers by testing how well does the autoencoder encodes and decodes an unseen audio file. This was done both by measuring difference of those files, as well as by listening to the input audio file as well as the audio that was encoded and than decoded.\par

Due to limitations of the system that I trained the neural nets on, I had to reduce the sampling rate of the data and therefore performance on raw data could be hindered. I will try to reflect this in the Section \ref{sec:results}.\par

All models where trained and run on Google Colab on the standard instance with GPU\footnote{Graphical processing unit}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Results
\section{Results}
\label{sec:results}

The input dataset was split into a training and validation part 70 to 30. All autoencoders, variational autoencoders where trained on 256 epochs and fullly connected neural nets used for classification where trained on 512 epochs using the training part of the dataset and their performance was measured on validation part of the dataset. I have chosen to have fix amount of epochs so that all models had the same amount of compute power and I could see how well they perform given this slightly restrictive environment.\par 

\begin{table}
\begin{tabular}{ |p{2.5cm}|p{2.5cm}|p{2.5cm}|  }
 \hline
 Data&Model&Final loss\\
 \hline
 Spectrograms&AE&0.0034\\
 Spectrograms&VAE&73.2654\\
 Raw&AE&0.6910\\
 Raw&VAE&2752.8701\\
 \hline
\end{tabular}
\caption{Performance of AEs and VAEs}
\label{table:ae_vae_perf}
\end{table}

Final loss for all four different autoencoders\footnote{Two deep autoencoders and two varientional autoencoders} can be seen in Table \ref{table:ae_vae_perf}. As we can see there is a vast difference in final loss between deep AEs and VAEs, however we cannot compare them directly. But we can compare performance between spectrogram data and raw data and it can be clearly seen that, spectrogram data performs far better than raw data. This can be also experienced by listening to the encoded and decoded examples that I have added to this report.I have a few theories for why this is happening.\par

First one is connected with the reduced sampling rate that I had to use for raw deep AE and raw VAE, as described in section \ref{sec:methods}. Due to this fact the amount of samples that contain the sound of the instrument itself is very small. Many audio samples have a large portion of their length mostly silent and that limits the number of samples with instrument sound even further. This could be potentially solved by increasing the sampling rate of the recording, which I sadly wasn't able to do.\par

Second one is related mostly to necessity of those models to train for much longer in order to achieve reasonable loss. This is mostly the case of the raw VAE, since the raw deep AE seems to have hit a plato. However this theory is far less likely that previously described theory, mainly due to the fact, that if we listen to the encoded and decoded audio sample of the raw deep AE it is very similar to the output of raw VAE and doesn't contain the key information.\par 

Nonetheless I have used all four autoencoders and encoded the complete validation data into their respective latent vectors representations. After that I have split those latent vector representations into train and validation part 70 to 30.\par

\begin{figure}
 \centering
 \includegraphics[scale=0.25]{img/models_comparison.png}
 \caption{Comparison of classification models performance}
 \label{fig:model_comp}
\end{figure} 

I have trained the same models for classification\footnote{Only change was input dimensions for FCNN}, as for feature extraction data, on training part of latent vectors representations and measured their accuracy on the validation part. Results of individual models for different autoencoders latent vector representations, as well as feature extraction representation, can be seen in Figure \ref{fig:model_comp}.\par 

We can see that the best accuracy has the random forest model on feature extraction data. Therefore the best accuracy was achieved not achieve on latent vector encoding. However what is interesting is the fact, that some models have much better accuracy on latent vector encoding, when compared to feature extraction data. As expected, latent vector encoding of raw data tends to perform much worse than spectogram data encoding, but surprisingly in some cases the performance is not as bad as I expected.\par 

Yet there is one more thing to take into account, and that is the fact that feature extraction models had much larger training set, due to the fact that I didn't need to use  part of the data for training of autoencoder. Latent vectors models only have 21\% of the training data, when compared to feature extraction models, but I have made sure that the distribution of individual classes stayed roughly the same.\par 

We can also see that deep AEs, tent to perform better than VAEs. I feared that this would happen and according to my research of this topic, this is known to happen.Based on that, I have chosen the random forest model on spectrogram data with deep autoencoder, since it provides best accuracy. I have estimated its "real-life" performance using a test dataset, that I had encoded it into latent vectors using the spectrogram deep AE and predicted their classes with random forrest. The test accuracy achieved by random forest was \textbf{0.4656}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Summary
\section{Summary}
To sum this up, I have explored the hypothesis about latent vector encoding using VAE having a positive effect on accuracy of prediction of musical instruments. Sadly this hypothesis doesn't seem to hold, since the accuracy of classification models on latent vectors from VAEs where outperformed both by deep AE baseline, as well as feature extraction baseline.\par

On the other hand, I believe that the potential of latent vector encoding is mayor, especially on a more complex data. The problem of classification of musical instruments seems to be way to simple and therefore the feature extraction can perform too good and it doesn't leave much wiggling room for improvement. It also can be seen that the accuracy on the encoded models tends to be more balanced. However from my experimentation it doesn't seem to adventitious to use VAE over a deep AE. Encoding also help the time necessary for training classification methods to be reduced. Those are reasons why I see encoding into latent vectors, as a good way of improving accuracy of classification, but with the added condition of high enough complexity, that the regular feature extraction is impossible or unfeasible.\par 

The best possible future expansion is most likely trying the performance of deep AE and VAE on raw data with their native non reduced sampling rate, provided that there is an access to a sufficient hardware. One more possible expansion could be focused on concatenating multiple audio samples of same instrument together in order to have larger samples and testing if this expansion in data per sample will help encoding to show its advantages more when compared to standard feature extraction.\par

All source code and part of the data is available publicly at \url{https://gitlab.fit.cvut.cz/vlktoma5/mvi-sp}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- Bibliography
%\bibliographystyle{plain-cz-online}
\bibliography{reference}

\end{document}
